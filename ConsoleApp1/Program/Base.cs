using System;
namespace ConsoleApp1
{
    public class CBase
    {
        public CBase() { }

        public void R_ASSERT(bool expr, string text)
        {
            if (!expr)
                throw new InvalidOperationException(text);
        }
    }
}