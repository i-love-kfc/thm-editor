﻿using System;
using Pfim;

namespace ConsoleApp1
{
	public class DDSImage
	{
		public IImage _image;

		public DDSImage(string file)
		{
			_image = Pfimage.FromFile(file);
			Process();
		}

		private void Process()
		{
			if (_image == null)
				throw new Exception("DDSImage image creation failed");
			_image.Decompress();
		}
	}
}
