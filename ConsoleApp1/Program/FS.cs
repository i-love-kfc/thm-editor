using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public partial class IReader : IDisposable
    {
        private const uint CHUNK_COMPRESSED = 0x80000000;
        private BinaryReader reader;

        public void Dispose()
        {
            reader.Dispose();
        }

        public IReader(BinaryReader r)
        {
            reader = r;
        }

        long m_last_pos = 0;
        public bool has_repaired_chunks = false;
        public uint find_chunk(int ID, string dbg_name)
        {
            uint dwSize = 0, dwType = 0;
            bool success = false;

            if (m_last_pos != 0 && m_last_pos + 8 <= reader.BaseStream.Length)
            {
                dwType = r_u32();
                dwSize = r_u32();
                if (dwType == ID || (dwType ^ CHUNK_COMPRESSED) == ID)
                {
                    success = true;
                }
            }
            if (!success)
            {
                reader.BaseStream.Position = 0;
                while (reader.BaseStream.Position + 8 <= reader.BaseStream.Length)
                {
                    dwType = r_u32();
                    dwSize = r_u32();
                    if (dwType == ID || (dwType ^ CHUNK_COMPRESSED) == ID)
                    {
                        success = true;
                        break;
                    }
                    else
                    {
                        if ((ID & 0x7ffffff0) == 0x810) // is it a thm chunk ID?
                        {
                            long pos = reader.BaseStream.Position;
                            long size = reader.BaseStream.Length;
                            uint length = dwSize;
                            if (pos + length != size) // not the last chunk in the file?
                            {
                                bool ok = true;
                                if (pos + length > size - 8) ok = false; // size too large?
                                if (ok)
                                {
                                    reader.BaseStream.Position = pos + length;
                                    if ((r_u32() & 0x7ffffff0) != 0x810) ok = false; // size too small?
                                }
                                if (!ok) // size incorrect?
                                {
                                    length = 0;
                                    while (pos + length < size) // find correct size, up to eof
                                    {
                                        reader.BaseStream.Position = pos + length;
                                        if (pos + length <= size - 8 && (r_u32() & 0x7ffffff0) == 0x810) break; // found start of next section
                                        length++;
                                    }
                                    string message = "!! THM " + dbg_name + ": chunk " + ID.ToString() + " fixed, wrong size = " + dwSize.ToString() + ", correct size = " + length.ToString();
                                    MessageBox.Show(message);
                                    has_repaired_chunks = true;
                                }
                            }
                            reader.BaseStream.Position = pos; // go back to beginning of chunk
                            dwSize = length; // use correct(ed) size
                        }
                        if (reader.BaseStream.Position + dwSize <= reader.BaseStream.Length)
                            reader.BaseStream.Position += dwSize;
                    }
                }
                if (!success)
                {
                    m_last_pos = 0;
                    return 0;
                }
            }
            long dwPos = reader.BaseStream.Position;
            if (dwPos + dwSize <= reader.BaseStream.Length)
            {
                m_last_pos = dwPos + dwSize;
            }
            else
            {
                m_last_pos = 0;
            }
            return dwSize;
        }

        public bool r_bool()
        {
            return r_u8() != 0;
        }
        public byte r_u8()
        {
            if (reader.BaseStream.Position + 1 > reader.BaseStream.Length)
                return 0;

            return reader.ReadByte();
        }
        public ushort r_u16()
        {
            if (reader.BaseStream.Position + 2 > reader.BaseStream.Length)
                return 0;

            return reader.ReadUInt16();
        }
        public uint r_u32()
        {
            if (reader.BaseStream.Position + 4 > reader.BaseStream.Length)
                return 0;

            return reader.ReadUInt32();
        }
        public int r_s32()
        {
            if (reader.BaseStream.Position + 4 > reader.BaseStream.Length)
                return 0;

            return reader.ReadInt32();
        }
        public float r_float()
        {
            if (reader.BaseStream.Position + 4 > reader.BaseStream.Length)
                return 0;

            return reader.ReadSingle();
        }
        public string r_stringZ()
        {
            string str = "";

            while (reader.BaseStream.Position < reader.BaseStream.Length)
            {
                byte[] one = { reader.ReadByte() };
                if (one[0] != 0)
                {
                    str += Encoding.Default.GetString(one);
                }
                else
                {
                    break;
                }
            }
            return str;
        }
    }
    public partial class IWriter : IDisposable
    {
        private long chunk_pos = 0;
        private BinaryWriter writer;

        public void Dispose()
        {
            writer.Dispose();
        }

        public IWriter(BinaryWriter w)
        {
            writer = w;
        }

        public void open_chunk(int chunkId)
        {
            w_u32((uint)chunkId);
            chunk_pos = writer.BaseStream.Position;
            w_u32(0);     // the place for 'size'
        }
        public void close_chunk()
        {
            if (chunk_pos == 0)
            {
                throw new InvalidOperationException("no chunk!");
            }

            long pos = writer.BaseStream.Position;
            writer.BaseStream.Position = chunk_pos;
            w_u32((uint)(pos - chunk_pos - 4));
            writer.BaseStream.Position = pos;
            chunk_pos = 0;
        }

        public void w_bool(bool write)
        {
            w_u8(write ? (byte)1 : (byte)0);
        }
        public void w_u8(byte write)
        {
            writer.Write(write);
        }
        public void w_u16(ushort write)
        {
            writer.Write(write);
        }
        public void w_u32(uint write)
        {
            writer.Write(write);
        }
        public void w_s32(int write)
        {
            writer.Write(write);
        }
        public void w_float(float write)
        {
            writer.Write(write);
        }
        public void w_stringZ(string text)
        {
            List<byte> temp = new List<byte>();

            temp.AddRange(Encoding.Default.GetBytes(text));
            temp.Add(0);

            writer.Write(temp.ToArray());
        }
    }
}