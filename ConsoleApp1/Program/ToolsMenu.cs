﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Pfim;

namespace ConsoleApp1
{
    using static ConsoleIO;

    public partial class ToolsMenu : Form
    {
        public ToolsMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // Починка тхм
        {
            if (folderBrowserDialog1.ShowDialog() != DialogResult.OK)
                return;

            var f = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.thm", SearchOption.AllDirectories);
            StartConsole();
            int files_fixed = 0;
            foreach (string i in f)
            {
                THM thm = new THM(null);
                thm.Load(i);

                if (thm.load_failed)
                {
                    Console.WriteLine("THM " + i + ": load failed, skipping");
                    continue;
                }

                if (thm.chunks_repaired)
                {
                    Console.WriteLine("THM " + i + ": fixed");
                    files_fixed++;
                    thm.Save(i);
                }
            }
            Console.WriteLine("Totally fixed: " + files_fixed.ToString() + " thms");
            EndConsole();
        }

        private void button2_Click(object sender, EventArgs e) // Изменение тхм формата (ТЧ/ЗП)
        {
            if (folderBrowserDialog1.ShowDialog() != DialogResult.OK)
                return;

            var f = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.thm", SearchOption.AllDirectories);
            StartConsole();

            Console.WriteLine("Convert to: (s - SOC format, c - SDK and COP format)");
            var selection = Console.Read();
            while (selection != 's' && selection != 'c' && selection != 'S' && selection != 'C')
                selection = Console.Read();
            bool soc_format = (selection == 's') || (selection == 'S');

            int files_conv = 0;
            foreach (string i in f)
            {
                THM thm = new THM(null);
                thm.Load(i);

                if (thm.load_failed)
                {
                    Console.WriteLine("THM " + i + ": load failed, skipping");
                    continue;
                }

                if (soc_format != thm.soc_repaired)
                {
                    thm.soc_repaired = soc_format;
					if(thm.bump_mode == THM.ETBumpMode.tbmUseParallax && soc_format)
						thm.bump_mode = THM.ETBumpMode.tbmUse;
					else if(thm.bump_mode == THM.ETBumpMode.tbmUse && !soc_format)
						thm.bump_mode = THM.ETBumpMode.tbmUseParallax;

                    Console.WriteLine("THM " + i + ": converted to " + (thm.soc_repaired ? "SOC format" : "COP format"));
                    files_conv++;
                    thm.Save(i);
                }
            }
            Console.WriteLine("Totally files converted: " + files_conv.ToString());
            EndConsole();
        }

        private void button3_Click(object sender, EventArgs e) // Проверка на соответствие тхм с текстурой
        {
            string pfim_dll_path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\Pfim.dll";
            if (!File.Exists(pfim_dll_path))
            {
                MessageBox.Show("Can't find Pfim.dll", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                StartThmValidate();
        }

        private void button4_Click(object sender, EventArgs e) // Генератор тхм файлов для текстур
        {
            string pfim_dll_path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\Pfim.dll";
            if (!File.Exists(pfim_dll_path))
            {
                MessageBox.Show("Can't find Pfim.dll", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            StartThmGenerateByDDS();
        }

        private void StartThmValidate()
        {
            var f = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.thm", SearchOption.AllDirectories);
            StartConsole();
            int valid_fail = 0, not_found = 0, open_failed = 0;
            foreach (string i in f)
            {
                THM thm = new THM(null);
                thm.Load(i);

                if (thm.load_failed)
                {
                    Console.WriteLine("THM " + i + ": load failed, skipping");
                    continue;
                }

                string i2 = i.TrimEnd("thm".ToCharArray()) + "dds";
                if (!File.Exists(i2))
                {
                    Console.WriteLine("DDS texture not found: " + i2);
                    not_found++;
                    continue;
                }

                try
                {
                    var dds_img = (Dds)new DDSImage(i2)._image;

                    if (!thm.ThmValidate(dds_img, i2.Substring(i2.LastIndexOf('\\') + 1)))
                    {
                        thm.GenerateThmForDds(dds_img, i2, i);

                        Console.WriteLine("THM doesn't correspond to the texture: " + i + ", fixed and saved");
                        valid_fail++;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Can't open DDS texture: " + i2);
                    open_failed++;
                    continue;
                }
            }
            Console.WriteLine("Totally validation fails: " + valid_fail.ToString());
            Console.WriteLine("Totally not found textures: " + not_found.ToString());
            Console.WriteLine("Totally problem textures: " + open_failed.ToString());
            EndConsole();
        }

        private void StartThmGenerateByDDS()
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                var dds_paths = Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.dds", SearchOption.AllDirectories);
                StartConsole();
                Console.WriteLine("Use SOC thm format? Y - yes, N - no");
                var key = Console.Read();
                while (key != 'Y' && key != 'N' && key != 'y' && key != 'n')
                    key = Console.Read();

                bool soc_format = ((key == 'Y') || (key == 'y'));

                int total_generated = 0, generate_failed = 0;
                foreach (string dds_path in dds_paths)
                {
                    if (dds_path.EndsWith("bump#.dds")) // Для bump# тхм не нужен
                        continue;

                    bool need_generate = false;

                    THM thm = new THM(null);
                    string thm_path = dds_path.TrimEnd("dds".ToCharArray()) + "thm";
                    if (File.Exists(thm_path))
                    {
                        thm.Load(thm_path);
                        if (thm.load_failed)
                            need_generate = true;
                    }
                    else
                        need_generate = true;

                    if (need_generate)
                    {
                        if (soc_format)
                            thm.soc_repaired = true;

                        try
                        {
                            var dds_img = (Dds)new DDSImage(dds_path)._image;
                            thm.GenerateThmForDds(dds_img, dds_path, thm_path);
                            Console.WriteLine("THM " + thm_path + ": generated successfully");
                            total_generated++;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Can't generate thm for DDS: " + dds_path);
                            generate_failed++;
                        }
                    }
                }
                Console.WriteLine("Totally generated thms: " + total_generated.ToString());
                Console.WriteLine("Totally fails: " + generate_failed.ToString());
                EndConsole();
            }
        }
    }
}
