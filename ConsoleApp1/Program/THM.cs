using System;
using System.IO;
using System.Windows.Forms;
using Pfim;

namespace ConsoleApp1
{
    public partial class THM : CBase
    {
        public enum ETType
        {
            ttImage = 0,
            ttCubeMap,
            ttBumpMap,
            ttNormalMap,
            ttTerrain,
        };
        public enum ETFormat
        {
            tfDXT1 = 0,
            tfADXT1,
            tfDXT3,
            tfDXT5,
            tf4444,
            tf1555,
            tf565,
            tfRGB,
            tfRGBA,
            tfNVHS,
            tfNVHU,
            tfA8,
            tfL8,
            tfA8L8,
        };
        public enum ETBumpMode
        {
            tbmAutogen = 0,
            tbmNone,
            tbmUse,
            tbmUseParallax,
        };
        public enum ETMaterial
        {
            tmOrenNayar_Blin = 0,
            tmBlin_Phong,
            tmPhong_Metal,
            tmMetal_OrenNayar,
        };

        [Flags]
        public enum ETextureFlags
        {
            flGenerateMipMaps = (1 << 0),
            flBinaryAlpha = (1 << 1),
            flAlphaBorder = (1 << 4),
            flColorBorder = (1 << 5),
            flFadeToColor = (1 << 6),
            flFadeToAlpha = (1 << 7),
            flDitherColor = (1 << 8),
            flDitherEachMIPLevel = (1 << 9),
            flGreyScale = (1 << 10),

            flDiffuseDetail = (1 << 23),
            flImplicitLighted = (1 << 24),
            flHasAlpha = (1 << 25),
            flBumpDetail = (1 << 26),
        };

        public enum EMIPFilters
        {
            kMIPFilterAdvanced = 5,

            kMIPFilterPoint = 2,
            kMIPFilterBox = 0,
            kMIPFilterTriangle = 3,
            kMIPFilterQuadratic = 4,
            kMIPFilterCubic = 1,

            kMIPFilterCatrom = 6,
            kMIPFilterMitchell = 7,

            kMIPFilterGaussian = 8,
            kMIPFilterSinc = 9,
            kMIPFilterBessel = 10,

            kMIPFilterHanning = 11,
            kMIPFilterHamming = 12,
            kMIPFilterBlackman = 13,
            kMIPFilterKaiser = 14,
        };

        public enum D3DFORMAT
        {
            D3DFMT_R8G8B8 = 20,
            D3DFMT_R5G6B5 = 23,
            D3DFMT_A1R5G5B5 = 25,
            D3DFMT_A4R4G4B4 = 26,
            D3DFMT_A8 = 28,
            D3DFMT_A8B8G8R8 = 32,

            D3DFMT_L8 = 50,
            D3DFMT_A8L8 = 51,

            D3DFMT_DXT1 = 827611204,
            D3DFMT_DXT3 = 861165636,
            D3DFMT_DXT5 = 894720068,
            D3DFMT_MULTI2_ARGB8 = 827606349,
        };

        public ETFormat fmt = ETFormat.tfDXT1;
        public ETType type = ETType.ttImage;
        public ETMaterial material = ETMaterial.tmOrenNayar_Blin;
        public ETBumpMode bump_mode = ETBumpMode.tbmNone;
        public EMIPFilters mip_filter = EMIPFilters.kMIPFilterKaiser;

        public uint border_color = 0, fade_color = 0, fade_amount = 0, width = 0, height = 0;
        public string detail_name = "", bump_name = "", ext_normal_map_name = "";
        public float material_weight = 0.5F, bump_virtual_height = 0.05F, detail_scale = 1;
        public ETextureFlags m_flags = ETextureFlags.flDitherColor;
        public byte fade_delay = 0;

        // В ТЧ и ЗП tp.fmt и tp.type перепутаны. СДК 0.4 и 0.7 их сохраняют всегда правильно (Как в зп).
        // Для тч использование в рендере tp.fmt вместо tp.type, вероятнее всего, является опечаткой, и как следствие,
        // неправильная загрузка THM файла. Вот такие вот приколы от ГСК.
        public bool soc_repaired = false;

        public bool chunks_repaired = false;

        const int THM_CHUNK_VERSION = 0x0810;
        const int THM_CHUNK_TEXTUREPARAM = 0x0812;
        const int THM_CHUNK_TYPE = 0x0813;
        const int THM_CHUNK_TEXTURE_TYPE = 0x0814;
        const int THM_CHUNK_DETAIL_EXT = 0x0815;
        const int THM_CHUNK_MATERIAL = 0x0816;
        const int THM_CHUNK_BUMP = 0x0817;
        const int THM_CHUNK_EXT_NORMALMAP = 0x0818;
        const int THM_CHUNK_FADE_DELAY = 0x0819;
        const int THM_CHUNK_THM_EDITOR_FLAG = 0x0820;

        private Form1 form;

        public THM(Form1 form1) { form = form1; }

        public void ResetValues()
        {
            soc_repaired = false;

            fmt = ETFormat.tfDXT1;
            type = ETType.ttImage;
            material = ETMaterial.tmOrenNayar_Blin;
            bump_mode = ETBumpMode.tbmNone;

            border_color = 0;
            fade_color = 0;
            fade_amount = 0;
            width = 0;
            height = 0;
            material_weight = 0.5F;
            bump_virtual_height = 0.05F;
            fade_delay = 0;
            detail_scale = 1;

            detail_name = "";
            bump_name = "";
            ext_normal_map_name = "";

            m_flags = ETextureFlags.flDitherColor;
        }

        public void OnTypeChange()
        {
            switch (type)
            {
                case ETType.ttBumpMap:
                    m_flags &= ~ETextureFlags.flGenerateMipMaps;
                    break;
                case ETType.ttNormalMap:
                    m_flags &= ~(ETextureFlags.flImplicitLighted | ETextureFlags.flBinaryAlpha | 
                        ETextureFlags.flAlphaBorder | ETextureFlags.flColorBorder | ETextureFlags.flFadeToColor |
                        ETextureFlags.flFadeToAlpha | ETextureFlags.flDitherColor | ETextureFlags.flDitherEachMIPLevel |
                        ETextureFlags.flBumpDetail);
                    m_flags |= ETextureFlags.flGenerateMipMaps;
                    mip_filter = EMIPFilters.kMIPFilterKaiser;
                    fmt = ETFormat.tfRGBA;
                    break;
                case ETType.ttTerrain:
                    m_flags &= ~(ETextureFlags.flGenerateMipMaps | ETextureFlags.flBinaryAlpha |
                        ETextureFlags.flAlphaBorder | ETextureFlags.flColorBorder | ETextureFlags.flFadeToColor |
                        ETextureFlags.flFadeToAlpha | ETextureFlags.flDitherColor | ETextureFlags.flDitherEachMIPLevel |
                        ETextureFlags.flBumpDetail);
                    m_flags |= ETextureFlags.flImplicitLighted;
                    fmt = ETFormat.tfDXT1;
                    break;
            }
            if(form != null)
                form.Form_Update();
        }

        public void GenerateThmForDds(Dds dds_img, string dds_path, string path = null)
        {
            string DDSfilename = dds_path.Substring(dds_path.LastIndexOf('\\') + 1);

            if (DDSfilename.StartsWith("terrain_"))
                type = ETType.ttTerrain;
            else if (DDSfilename.EndsWith("_bump.dds"))
                type = ETType.ttBumpMap;
            else if (DDSfilename.EndsWith("_nmap.dds"))
                type = ETType.ttNormalMap;
            OnTypeChange();

            width = (uint)dds_img.Width;
            height = (uint)dds_img.Height;

            if (type != ETType.ttNormalMap && type != ETType.ttTerrain)
            {
                D3DFORMAT FourCC = (D3DFORMAT)dds_img.Header.PixelFormat.FourCC;

                switch (FourCC)
                {
                    case D3DFORMAT.D3DFMT_DXT1:
                        fmt = dds_img.Data[34] != 0 ? ETFormat.tfADXT1 : ETFormat.tfDXT1;
                        break;
                    case D3DFORMAT.D3DFMT_DXT3:
                        fmt = ETFormat.tfDXT3;
                        break;
                    case D3DFORMAT.D3DFMT_DXT5:
                        fmt = ETFormat.tfDXT5;
                        break;
                    case D3DFORMAT.D3DFMT_MULTI2_ARGB8:
                        fmt = ETFormat.tfRGBA;
                        break;
                    case D3DFORMAT.D3DFMT_A8B8G8R8:
                        fmt = ETFormat.tfRGBA;
                        break;
                    case D3DFORMAT.D3DFMT_A8:
                        fmt = ETFormat.tfA8;
                        break;
                    case D3DFORMAT.D3DFMT_L8:
                        fmt = ETFormat.tfL8;
                        break;
                    case D3DFORMAT.D3DFMT_A8L8:
                        fmt = ETFormat.tfA8L8;
                        break;
                    case D3DFORMAT.D3DFMT_A4R4G4B4:
                        fmt = ETFormat.tf4444;
                        break;
                    case D3DFORMAT.D3DFMT_A1R5G5B5:
                        fmt = ETFormat.tf1555;
                        break;
                    case D3DFORMAT.D3DFMT_R5G6B5:
                        fmt = ETFormat.tf565;
                        break;
                    case D3DFORMAT.D3DFMT_R8G8B8:
                        fmt = ETFormat.tfRGB;
                        break;
                }
            }

            if (type != ETType.ttBumpMap && type != ETType.ttNormalMap && type != ETType.ttTerrain)
            {
                if (dds_img.Header.MipMapCount > 0)
                    m_flags |= ETextureFlags.flGenerateMipMaps;
                else
                    m_flags &= ~ETextureFlags.flGenerateMipMaps;
            }

            if (dds_img.Data[34] != 0)
                m_flags |= ETextureFlags.flHasAlpha;
            else
                m_flags &= ~ETextureFlags.flHasAlpha;

            string bump_path = dds_path.TrimEnd(".dds".ToCharArray()) + "_bump.dds";
            string nmap_path = dds_path.TrimEnd(".dds".ToCharArray()) + "_nmap.dds";
            string det_path = dds_path.TrimEnd(".dds".ToCharArray()) + "_det.dds";

            if (File.Exists(bump_path))
            {
                bump_name = bump_path.Substring(bump_path.LastIndexOf("textures") + 9).TrimEnd(".dds".ToCharArray());
                bump_mode = !soc_repaired ? ETBumpMode.tbmUseParallax : ETBumpMode.tbmUse;
            }
            else if(dds_path.EndsWith("diff.dds"))
            {
                bump_path = dds_path.TrimEnd("diff.dds".ToCharArray()) + "_bump.dds";
                if (File.Exists(bump_path))
                {
                    bump_name = bump_path.Substring(bump_path.LastIndexOf("textures") + 9).TrimEnd(".dds".ToCharArray());
                    bump_mode = !soc_repaired ? ETBumpMode.tbmUseParallax : ETBumpMode.tbmUse;
                }
            }
            if (File.Exists(nmap_path))
            {
                ext_normal_map_name = nmap_path.Substring(nmap_path.LastIndexOf("textures") + 9).TrimEnd(".dds".ToCharArray());
            }
            if (File.Exists(det_path))
            {
                detail_name = det_path.Substring(det_path.LastIndexOf("textures") + 9).TrimEnd(".dds".ToCharArray());
            }

            if (path != null)
                Save(path);
        }
        public bool ThmValidate(Dds image, string filename)
        {
            if (width != (uint)image.Width)
                return false;

            if (height != (uint)image.Height)
                return false;

            if (filename.StartsWith("terrain_") && type != ETType.ttTerrain)
                return false;
            else if (filename.EndsWith("_bump.dds") && type != ETType.ttBumpMap)
                return false;
            else if (filename.EndsWith("_nmap.dds") && type != ETType.ttNormalMap)
                return false;

            if (type != ETType.ttBumpMap && type != ETType.ttNormalMap && type != ETType.ttTerrain && ((image.Header.MipMapCount > 0) != m_flags.HasFlag(ETextureFlags.flGenerateMipMaps)))
                return false;
            else
            {
                if ((type == ETType.ttBumpMap || type == ETType.ttTerrain) && m_flags.HasFlag(ETextureFlags.flGenerateMipMaps))
                    return false;
                else if(type == ETType.ttNormalMap && !m_flags.HasFlag(ETextureFlags.flGenerateMipMaps))
                    return false;
            }

            if ((image.Data[34] != 0) != m_flags.HasFlag(ETextureFlags.flHasAlpha))
                return false;

            if (type != ETType.ttNormalMap && type != ETType.ttTerrain)
            {
                D3DFORMAT FourCC = (D3DFORMAT)image.Header.PixelFormat.FourCC;

                switch (FourCC)
                {
                    case D3DFORMAT.D3DFMT_DXT1:
                        if (fmt != (image.Data[34] != 0 ? ETFormat.tfADXT1 : ETFormat.tfDXT1))
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_DXT3:
                        if (fmt != ETFormat.tfDXT3)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_DXT5:
                        if (fmt != ETFormat.tfDXT5)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_MULTI2_ARGB8:
                        if (fmt != ETFormat.tfRGBA)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_A8B8G8R8:
                        if (fmt != ETFormat.tfRGBA)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_A8:
                        if (fmt != ETFormat.tfA8)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_L8:
                        if (fmt != ETFormat.tfL8)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_A8L8:
                        if (fmt != ETFormat.tfA8L8)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_A4R4G4B4:
                        if (fmt != ETFormat.tf4444)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_A1R5G5B5:
                        if (fmt != ETFormat.tf1555)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_R5G6B5:
                        if (fmt != ETFormat.tf565)
                        {
                            return false;
                        }
                        break;
                    case D3DFORMAT.D3DFMT_R8G8B8:
                        if (fmt != ETFormat.tfRGB)
                        {
                            return false;
                        }
                        break;
                }
            }
            else
            {
                if (type == ETType.ttNormalMap)
                {
                    if (fmt != ETFormat.tfRGBA)
                        return false;
                    if (mip_filter != EMIPFilters.kMIPFilterKaiser)
                        return false;
                }
                else if (type == ETType.ttTerrain && fmt != ETFormat.tfDXT1)
                    return false;
            }

            switch(type)
            {
                case ETType.ttBumpMap:
                    if (m_flags.HasFlag(ETextureFlags.flGenerateMipMaps))
                        return false;

                    break;
                case ETType.ttNormalMap:
                    if (m_flags.HasFlag(ETextureFlags.flImplicitLighted | ETextureFlags.flBinaryAlpha |
                        ETextureFlags.flAlphaBorder | ETextureFlags.flColorBorder | ETextureFlags.flFadeToColor |
                        ETextureFlags.flFadeToAlpha | ETextureFlags.flDitherColor | ETextureFlags.flDitherEachMIPLevel |
                        ETextureFlags.flBumpDetail))
                            return false;

                    if (!m_flags.HasFlag(ETextureFlags.flGenerateMipMaps))
                        return false;

                    break;
                case ETType.ttTerrain:
                    if (m_flags.HasFlag(ETextureFlags.flGenerateMipMaps | ETextureFlags.flBinaryAlpha |
                        ETextureFlags.flAlphaBorder | ETextureFlags.flColorBorder | ETextureFlags.flFadeToColor |
                        ETextureFlags.flFadeToAlpha | ETextureFlags.flDitherColor | ETextureFlags.flDitherEachMIPLevel |
                        ETextureFlags.flBumpDetail))
                        return false;

                    if (!m_flags.HasFlag(ETextureFlags.flImplicitLighted))
                        return false;

                    break;
            }

            return true;
        }

        public bool load_failed = false;
        public void Load(string filename)
        {
            ResetValues();
            using (IReader reader = new IReader(new BinaryReader(File.Open(filename, FileMode.Open))))
            {
                if (form != null)
                    form.need_update_values = false;

                if (reader.find_chunk(THM_CHUNK_THM_EDITOR_FLAG, filename) != 0)
                {
                    soc_repaired = reader.r_bool();
                }
                if (reader.find_chunk(THM_CHUNK_TYPE, filename) >= 5)
                {
                    reader.r_u32();
                    soc_repaired = reader.r_bool();
                }
                if (reader.find_chunk(THM_CHUNK_TEXTUREPARAM, filename) == 0)
                {
                    if (form == null) // Tools menu
                    {
                        Console.WriteLine("THM " + filename + ": chunk THM_CHUNK_TEXTUREPARAM not found, skipping");
                        load_failed = true;
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Failed to load THM: chunk THM_CHUNK_TEXTUREPARAM not found");
                        form.Values_Update();
                    }
                }
                if (soc_repaired)
                    type = (ETType)reader.r_s32();
                else
                    fmt = (ETFormat)reader.r_s32();

                m_flags = (ETextureFlags)reader.r_s32();
                border_color = reader.r_u32();
                fade_color = reader.r_u32();
                fade_amount = reader.r_u32();
                mip_filter = (EMIPFilters)reader.r_s32();
                width = reader.r_u32();
                height = reader.r_u32();

                if (reader.find_chunk(THM_CHUNK_TEXTURE_TYPE, filename) != 0)
                {
                    if (soc_repaired)
                        fmt = (ETFormat)reader.r_s32();
                    else
                        type = (ETType)reader.r_s32();
                }
                if (reader.find_chunk(THM_CHUNK_DETAIL_EXT, filename) != 0)
                {
                    detail_name = reader.r_stringZ();
                    detail_scale = reader.r_float();
                }
                if (reader.find_chunk(THM_CHUNK_MATERIAL, filename) != 0)
                {
                    material = (ETMaterial)reader.r_s32();
                    material_weight = reader.r_float();
                }
                if (reader.find_chunk(THM_CHUNK_BUMP, filename) != 0)
                {
                    bump_virtual_height = reader.r_float();
                    bump_mode = (ETBumpMode)reader.r_s32();
                    bump_name = reader.r_stringZ();
                }
                if (reader.find_chunk(THM_CHUNK_EXT_NORMALMAP, filename) != 0)
                {
                    ext_normal_map_name = reader.r_stringZ();
                }
                if (reader.find_chunk(THM_CHUNK_FADE_DELAY, filename) != 0)
                {
                    fade_delay = reader.r_u8();
                }

                chunks_repaired = reader.has_repaired_chunks;
            }
        }
        public void Save(string filename)
        {
            if (form != null)
                form.Values_Update();
            using (IWriter writer = new IWriter(new BinaryWriter(File.Create(filename))))
            {
                writer.open_chunk(THM_CHUNK_VERSION);
                writer.w_u16((ushort)0x0012);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_TYPE);
                writer.w_u32(1);
                writer.w_bool(soc_repaired);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_TEXTUREPARAM);
                writer.w_s32(soc_repaired ? (int)type : (int)fmt);
                writer.w_s32((int)m_flags);
                writer.w_u32(border_color);
                writer.w_u32(fade_color);
                writer.w_u32(fade_amount);
                writer.w_s32((int)mip_filter);
                writer.w_u32(width);
                writer.w_u32(height);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_TEXTURE_TYPE);
                writer.w_s32(soc_repaired ? (int)fmt : (int)type);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_DETAIL_EXT);
                writer.w_stringZ(detail_name);
                writer.w_float(detail_scale);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_MATERIAL);
                writer.w_s32((int)material);
                writer.w_float(material_weight);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_BUMP);
                writer.w_float(bump_virtual_height);
                writer.w_s32(soc_repaired && bump_mode == ETBumpMode.tbmUseParallax ? (int)ETBumpMode.tbmUse : (int)bump_mode);
                writer.w_stringZ(bump_name);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_EXT_NORMALMAP);
                writer.w_stringZ(ext_normal_map_name);
                writer.close_chunk();

                writer.open_chunk(THM_CHUNK_FADE_DELAY);
                writer.w_u8(fade_delay);
                writer.close_chunk();
            }
        }
    }
}