using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Pfim;
using System.IO;

namespace ConsoleApp1
{
    public partial class Form1 : Form
    {
        private THM thm;
        public bool need_update_values = false;

        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = saveFileDialog1.Filter = "THM file|*.thm";
            openFileDialog2.Filter = "DDS texture|*.dds";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;

            thm = new THM(this);
            Form_Update();
            need_update_values = true;

            if (Program.IsDebugMode())
                this.Text = "THM Editor (Debug Mode)";
        }

        public void Form_Update()
        {
            comboBox1.SelectedIndex = (int)thm.type;
            comboBox2.SelectedIndex = (int)thm.fmt;
            comboBox3.SelectedIndex = (int)thm.bump_mode;
            comboBox4.SelectedIndex = (int)thm.mip_filter;
            comboBox5.SelectedIndex = (int)thm.material;

            textBox4.Text = thm.border_color.ToString();
            textBox6.Text = thm.fade_color.ToString();
            textBox5.Text = thm.fade_amount.ToString();
            textBox8.Text = thm.width.ToString();
            textBox9.Text = thm.height.ToString();
            textBox10.Text = thm.fade_delay.ToString();
            textBox11.Text = thm.material_weight.ToString();
            textBox12.Text = thm.bump_virtual_height.ToString();
            textBox7.Text = thm.detail_scale.ToString();
            textBox1.Text = thm.detail_name;
            textBox2.Text = thm.bump_name;
            textBox3.Text = thm.ext_normal_map_name;
            checkBox1.Checked = thm.soc_repaired;

            comboBox2.Enabled = thm.type != THM.ETType.ttNormalMap && thm.type != THM.ETType.ttTerrain;
            comboBox4.Enabled = thm.type != THM.ETType.ttNormalMap;
        }
        public void Values_Update()
        {
            thm.type = (THM.ETType)comboBox1.SelectedIndex;
            thm.fmt = (THM.ETFormat)comboBox2.SelectedIndex;
            thm.bump_mode = (THM.ETBumpMode)comboBox3.SelectedIndex;
            thm.mip_filter = (THM.EMIPFilters)comboBox4.SelectedIndex;
            thm.material = (THM.ETMaterial)comboBox5.SelectedIndex;

            thm.border_color = Convert.ToUInt32(textBox4.Text);
            thm.fade_color = Convert.ToUInt32(textBox6.Text);
            thm.fade_amount = Convert.ToUInt32(textBox5.Text);
            thm.width = Convert.ToUInt32(textBox8.Text);
            thm.height = Convert.ToUInt32(textBox9.Text);
            thm.fade_delay = Convert.ToByte(textBox10.Text);
            thm.material_weight = Convert.ToSingle(textBox11.Text);
            thm.bump_virtual_height = Convert.ToSingle(textBox12.Text);
            thm.detail_scale = Convert.ToSingle(textBox7.Text);
            thm.detail_name = textBox1.Text;
            thm.bump_name = textBox2.Text;
            thm.ext_normal_map_name = textBox3.Text;
            thm.soc_repaired = checkBox1.Checked;
        }

        private void button1_Click(object sender, EventArgs e) // Open .thm
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                thm.Load(openFileDialog1.FileName);
                Form_Update();

                label20.Text = openFileDialog1.FileName.Substring(openFileDialog1.FileName.LastIndexOf('\\') + 1);
                string pfim_dll_path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\Pfim.dll";
                if (File.Exists(pfim_dll_path))
                {
                    string dds_path = openFileDialog1.FileName.TrimEnd(".thm".ToCharArray()) + ".dds";
                    AddTextureToBox(dds_path);
                }
            }
        }
        private void button2_Click(object sender, EventArgs e) // Save as
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                thm.Save(saveFileDialog1.FileName);

                label20.Text = saveFileDialog1.FileName.Substring(saveFileDialog1.FileName.LastIndexOf('\\') + 1);
                string pfim_dll_path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\Pfim.dll";
                if (File.Exists(pfim_dll_path))
                {
                    string dds_path = saveFileDialog1.FileName.TrimEnd(".thm".ToCharArray()) + ".dds";
                    AddTextureToBox(dds_path);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e) // Save
        {
            if (saveFileDialog1.FileName != null && File.Exists(saveFileDialog1.FileName))
                thm.Save(saveFileDialog1.FileName);
            else if (openFileDialog1.FileName != null && File.Exists(openFileDialog1.FileName))
                thm.Save(openFileDialog1.FileName);
            else
                button2_Click(sender, e);
        }

        private void button4_Click(object sender, EventArgs e) // Import DDS
        {
            string pfim_dll_path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf('\\')) + "\\Pfim.dll";
            if (File.Exists(pfim_dll_path))
            {
                if (openFileDialog2.ShowDialog() != DialogResult.OK)
                    return;
                ImportDDS();
            }
            else
                MessageBox.Show("Can't find Pfim.dll", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ImportDDS()
        {
            try
            {
                var dds_img = (Dds)new DDSImage(openFileDialog2.FileName)._image;
                thm.GenerateThmForDds(dds_img, openFileDialog2.FileName);
                Form_Update();

                AddTextureToBox(openFileDialog2.FileName);
            }
            catch (Exception)
            {
                MessageBox.Show("Can't import texture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddTextureToBox(string path)
        {
            if (File.Exists(path))
            {
                var dds_img = new DDSImage(path)._image;
                var ptr = Marshal.UnsafeAddrOfPinnedArrayElement(dds_img.Data, 0);
                var bitmap = new Bitmap(dds_img.Width, dds_img.Height, dds_img.Stride, PixelFormat.Format32bppArgb, ptr);

                pictureBox1.Image = bitmap;
            }
            else
                pictureBox1.Image = null;
        }

        private void button5_Click(object sender, EventArgs e) // Edit flags
        {
            FlagsMenu flagsmenu = new FlagsMenu(thm);
            flagsmenu.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e) // Tools
        {
            ToolsMenu menu = new ToolsMenu();
            menu.ShowDialog();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) // Texture type
        {
            if (need_update_values)
                Values_Update();
            thm.OnTypeChange();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) // SOC format
        {
            thm.soc_repaired = checkBox1.Checked;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.S) // ���������� � ������� �������� ����
                button3_Click(sender, e);

            switch (e.KeyData)
            {
                case Keys.F4: button1_Click(sender, e); break; // �������� �����
                case Keys.F5: button3_Click(sender, e); break; // ���������� � ������� �������� ����
                case Keys.F6: button2_Click(sender, e); break; // ���������� � ����� ����
            }
        }
    }
}
