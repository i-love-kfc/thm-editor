using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public static class ConsoleIO
    {
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern bool system(string str);

        [DllImport("kernel32.dll")]
        private static extern int AllocConsole();

        [DllImport("kernel32.dll")]
        private static extern int FreeConsole();

        public static void StartConsole()
        {
            AllocConsole();
            TextWriter stdOutWriter = new StreamWriter(Console.OpenStandardOutput(), Console.OutputEncoding) { AutoFlush = true };
            TextWriter stdErrWriter = new StreamWriter(Console.OpenStandardError(), Console.OutputEncoding) { AutoFlush = true };
            TextReader strInReader = new StreamReader(Console.OpenStandardInput(), Console.InputEncoding);
            Console.SetOut(stdOutWriter);
            Console.SetError(stdErrWriter);
            Console.SetIn(strInReader);
        }

        public static void EndConsole()
        {
            system("pause");
            FreeConsole();
        }
    }

    internal class Program
    {
        public static bool IsDebugMode()
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }

        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
