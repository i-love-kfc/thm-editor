﻿using System;
using System.Windows.Forms;

namespace ConsoleApp1
{
    public partial class FlagsMenu : Form
    {
        private THM thm;
        private bool need_update = false;

        public FlagsMenu(THM cast_thm)
        {
            thm = cast_thm;
            InitializeComponent();
        }

        private void FlagsMenu_Load(object sender, EventArgs e)
        {
            need_update = false;

            checkBox1.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flGenerateMipMaps);
            checkBox2.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flBinaryAlpha);
            checkBox3.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flAlphaBorder);
            checkBox4.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flColorBorder);
            checkBox5.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flFadeToColor);
            checkBox6.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flFadeToAlpha);
            checkBox7.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flDitherColor);
            checkBox8.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flDitherEachMIPLevel);
            checkBox9.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flDiffuseDetail);
            checkBox10.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flImplicitLighted);
            checkBox11.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flHasAlpha);
            checkBox12.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flBumpDetail);
            checkBox13.Checked = thm.m_flags.HasFlag(THM.ETextureFlags.flGreyScale);

            bool enable = thm.type != THM.ETType.ttNormalMap && thm.type != THM.ETType.ttTerrain;
            checkBox1.Enabled = thm.type != THM.ETType.ttBumpMap && enable;
            checkBox2.Enabled = enable;
            checkBox3.Enabled = enable;
            checkBox4.Enabled = enable;
            checkBox5.Enabled = enable;
            checkBox6.Enabled = enable;
            checkBox7.Enabled = enable;
            checkBox8.Enabled = enable;
            checkBox10.Enabled = enable;
            checkBox12.Enabled = enable;

            need_update = true;
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!need_update)
                return;

            if (checkBox1.Checked)
                thm.m_flags |= THM.ETextureFlags.flGenerateMipMaps;
            else
                thm.m_flags &= ~THM.ETextureFlags.flGenerateMipMaps;

            if (checkBox2.Checked)
                thm.m_flags |= THM.ETextureFlags.flBinaryAlpha;
            else
                thm.m_flags &= ~THM.ETextureFlags.flBinaryAlpha;

            if (checkBox3.Checked)
                thm.m_flags |= THM.ETextureFlags.flAlphaBorder;
            else
                thm.m_flags &= ~THM.ETextureFlags.flAlphaBorder;

            if (checkBox4.Checked)
                thm.m_flags |= THM.ETextureFlags.flColorBorder;
            else
                thm.m_flags &= ~THM.ETextureFlags.flColorBorder;

            if (checkBox5.Checked)
                thm.m_flags |= THM.ETextureFlags.flFadeToColor;
            else
                thm.m_flags &= ~THM.ETextureFlags.flFadeToColor;

            if (checkBox6.Checked)
                thm.m_flags |= THM.ETextureFlags.flFadeToAlpha;
            else
                thm.m_flags &= ~THM.ETextureFlags.flFadeToAlpha;

            if (checkBox7.Checked)
                thm.m_flags |= THM.ETextureFlags.flDitherColor;
            else
                thm.m_flags &= ~THM.ETextureFlags.flDitherColor;

            if (checkBox8.Checked)
                thm.m_flags |= THM.ETextureFlags.flDitherEachMIPLevel;
            else
                thm.m_flags &= ~THM.ETextureFlags.flDitherEachMIPLevel;

            if (checkBox9.Checked)
                thm.m_flags |= THM.ETextureFlags.flDiffuseDetail;
            else
                thm.m_flags &= ~THM.ETextureFlags.flDiffuseDetail;

            if (checkBox10.Checked)
                thm.m_flags |= THM.ETextureFlags.flImplicitLighted;
            else
                thm.m_flags &= ~THM.ETextureFlags.flImplicitLighted;

            if (checkBox11.Checked)
                thm.m_flags |= THM.ETextureFlags.flHasAlpha;
            else
                thm.m_flags &= ~THM.ETextureFlags.flHasAlpha;

            if (checkBox12.Checked)
                thm.m_flags |= THM.ETextureFlags.flBumpDetail;
            else
                thm.m_flags &= ~THM.ETextureFlags.flBumpDetail;

            if (checkBox13.Checked)
                thm.m_flags |= THM.ETextureFlags.flGreyScale;
            else
                thm.m_flags &= ~THM.ETextureFlags.flGreyScale;
        }
    }
}
